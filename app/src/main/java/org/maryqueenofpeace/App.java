package org.maryqueenofpeace;

import android.app.Application;
import android.view.View;

import org.maryqueenofpeace.db.LocalDataBase;
import org.maryqueenofpeace.model.ContactsModal;

import java.util.ArrayList;
import java.util.List;

public class App extends Application {
    public static List<ContactsModal> contactsModalList = new ArrayList<>();

    @Override
    public void onCreate() {
        super.onCreate();
    }

    public void loadContacts(RepoInterface<String> repoInterface) {
        LocalDataBase.databaseWriterService.execute(() -> {
            contactsModalList = ContactsHelper.getContacts(App.this);
            repoInterface.onSuccess("SUCCESS");
        });
    }
}
