package org.maryqueenofpeace.db.retrofit;


import org.maryqueenofpeace.model.ContactsModal;

import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface ApiInterface {

    @GET("customers")
    Call<List<ContactsModal>> getContacts();

    @FormUrlEncoded
    @POST("customers")
    Call<ResponseBody> saveData(
            @Field("school_id") String school_id,
            @Field("contacts[]") List<String> contacts
    );

}