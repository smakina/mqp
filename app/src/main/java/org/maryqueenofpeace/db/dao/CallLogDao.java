package org.maryqueenofpeace.db.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import org.maryqueenofpeace.model.MCallLog;

import java.util.List;


@Dao
public interface CallLogDao {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insert(MCallLog... MCallLog);

    @Query("SELECT * FROM tbl_cal_log WHERE type != 'OUTGOING' ORDER BY id DESC ")
    LiveData<List<MCallLog>> getCallLog();

    @Query("SELECT count(*) FROM tbl_cal_log WHERE type =:filter")
    LiveData<Integer> getCount(String filter);

    @Query("SELECT count(*) FROM tbl_cal_log WHERE type != 'OUTGOING'")
    LiveData<Integer> getCountTotal();

}
