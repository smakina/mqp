package org.maryqueenofpeace.db.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import org.maryqueenofpeace.model.ContactsModal;

import java.util.List;

@Dao
public interface ContactsDao {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insert(ContactsModal...contactsModals);

    @Query("SELECT * FROM tbl_contacts")
    LiveData<List<ContactsModal>> getContacts();

}
