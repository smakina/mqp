package org.maryqueenofpeace.utils;

import android.content.Context;
import android.text.method.LinkMovementMethod;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import androidx.appcompat.widget.AppCompatButton;

import org.maryqueenofpeace.DialogInterface;
import org.maryqueenofpeace.R;

public class Dialog {

    public static void dialogInfo(
            Context context,
            String title,
            String content,
            DialogInterface dialogInterface
    ) {

        final android.app.Dialog dialog = new android.app.Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);  // before
        dialog.setContentView(R.layout.dialog_info);
        dialog.setCancelable(true);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;

        ((TextView) dialog.findViewById(R.id.title)).setText(title);
        ((TextView) dialog.findViewById(R.id.content)).setText(content);

        ((AppCompatButton) dialog.findViewById(R.id.bt_close))
                .setOnClickListener((View.OnClickListener) v -> {
                    if (dialogInterface != null) {
                        dialogInterface.dialogOk();
                    }
                    dialog.dismiss();
                });

        dialog.show();
        dialog.getWindow().setAttributes(lp);
    }

//    public static void dialogInfo(
//            Context context,
//            String title,
//            String content,
//            DialogInterface dialogInterface,
//            boolean closeable
//    ) {
//
//        final android.app.Dialog dialog = new android.app.Dialog(context);
//        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);  // before
//        dialog.setContentView(R.layout.dialog_info);
//        dialog.setCancelable(true);
//
//        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
//        lp.copyFrom(dialog.getWindow().getAttributes());
//        lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
//        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
//
//        ((TextView) dialog.findViewById(R.id.title)).setText(title);
//        ((TextView) dialog.findViewById(R.id.content)).setText(content);
//
//        ((AppCompatButton) dialog.findViewById(R.id.bt_close))
//                .setOnClickListener((View.OnClickListener) v -> {
//                    if (dialogInterface != null) {
//                        dialogInterface.dialogOk();
//                    }
//                    dialog.dismiss();
//                });
//
//        if (closeable) {
//            dialog.setCancelable(false);
//        }
//        dialog.show();
//        dialog.getWindow().setAttributes(lp);
//    }
//
    public static void dialogError(
            Context context,
            String title,
            String content,
            DialogInterface dialogInterface
    ) {

        final android.app.Dialog dialog = new android.app.Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // before
        dialog.setContentView(R.layout.dialog_error);
        dialog.setCancelable(true);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;

        ((TextView) dialog.findViewById(R.id.title)).setText(title);
        ((TextView) dialog.findViewById(R.id.content)).setText(content);
        AppCompatButton closeBtn = dialog.findViewById(R.id.bt_close);

        if (dialogInterface != null) {
            closeBtn.setText("Retry");
//            dialog.setCancelable(false);
        }

        closeBtn.setOnClickListener((View.OnClickListener) v -> {
            if (dialogInterface != null) {
                dialogInterface.dialogOk();
            }
            dialog.dismiss();
        });


        dialog.show();
        dialog.getWindow().setAttributes(lp);
    }

//
//
//    public static void dialogDelete(
//            Context context,
//            String title,
//            String content,
//            DialogInterface dialogInterface
//    ) {
//
//        final android.app.Dialog dialog = new android.app.Dialog(context);
//        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // before
//        dialog.setContentView(R.layout.dialog_error);
//        dialog.setCancelable(true);
//
//        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
//        lp.copyFrom(dialog.getWindow().getAttributes());
//        lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
//        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
//
//        ((TextView) dialog.findViewById(R.id.title)).setText(title);
//        ((TextView) dialog.findViewById(R.id.content)).setText(content);
//        AppCompatButton closeBtn = dialog.findViewById(R.id.bt_close);
//
//        if (dialogInterface != null) {
//            closeBtn.setText(R.string.yes_proceed);
//        }
//
//        closeBtn.setOnClickListener(v -> {
//            if (dialogInterface != null) {
//                dialogInterface.dialogOk();
//            }
//            dialog.dismiss();
//        });
//
//        dialog.show();
//        dialog.getWindow().setAttributes(lp);
//    }
//
//    public static void dialogYesOrNo(
//            Context context,
//            String title,
//            String content,
//            DialogYesNo dialogInterface
//    ) {
//
//        final android.app.Dialog dialog = new android.app.Dialog(context);
//        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // before
//        dialog.setContentView(R.layout.dialog_yes_no);
//        dialog.setCancelable(true);
//
//        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
//        lp.copyFrom(dialog.getWindow().getAttributes());
//        lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
//        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
//
//        ((TextView) dialog.findViewById(R.id.title)).setText(title);
//        ((TextView) dialog.findViewById(R.id.content)).setText(content);
//        AppCompatButton yesBtn = dialog.findViewById(R.id.bt_yes);
//        AppCompatButton noBtn = dialog.findViewById(R.id.bt_no);
//
//
//        yesBtn.setOnClickListener((View.OnClickListener) v -> {
//            if (dialogInterface != null) {
//                dialogInterface.yes();
//            }
//            dialog.dismiss();
//        });
//
//        noBtn.setOnClickListener((View.OnClickListener) v -> {
//            if (dialogInterface != null) {
//                dialogInterface.no();
//            }
//            dialog.dismiss();
//        });
//
//        dialog.show();
//        dialog.getWindow().setAttributes(lp);
//    }
//
//    public static void showMessage(Context context, String message) {
//        final android.app.Dialog dialog = new android.app.Dialog(context);
//        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // before
//        dialog.setContentView(R.layout.dialog_message);
//        dialog.setCancelable(true);
//        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
//        lp.copyFrom(dialog.getWindow().getAttributes());
//        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
//        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
//
//        ((TextView) dialog.findViewById(R.id.tv_content)).setMovementMethod(LinkMovementMethod.getInstance());
//        ((TextView) dialog.findViewById(R.id.tv_content)).setText(message);
//        dialog.findViewById(R.id.bt_accept).setOnClickListener(v -> dialog.dismiss());
//        dialog.show();
//        dialog.getWindow().setAttributes(lp);
//    }

//    public static void showImageDialog(Context context, String url) {
//        final android.app.Dialog dialog = new android.app.Dialog(context);
//        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // before
//        dialog.setContentView(R.layout.dialog_image);
//        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
//        dialog.setCancelable(true);
//        dialog.setOnShowListener(dialogInterface -> {
//            ImageView imageView = dialog.findViewById(R.id.image_view);
//            Glide.with(context).load(url).error(R.drawable.ic_baseline_broken_image_24).into(imageView);
//        });
//        dialog.show();
//    }

//    private void showDialogImageCenter() {
//        final android.app.Dialog dialog = new android.app.Dialog(this);
//        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // before
//        dialog.setContentView(R.layout.dialog_image_center);
//        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
//        dialog.setCancelable(true);
//        dialog.show();
//    }
//
//    private void showDialogImageShare() {
//        final android.app.Dialog dialog = new android.app.Dialog(this);
//        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // before
//        dialog.setContentView(R.layout.dialog_image_share);
//        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
//        dialog.setCancelable(true);
//        dialog.show();
//    }
//
//    private void showDialogImageQuotes() {
//        final android.app.Dialog dialog = new android.app.Dialog(this);
//        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // before
//        dialog.setContentView(R.layout.dialog_image_quotes);
//        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
//        dialog.setCancelable(true);
//        dialog.show();
//    }
}
