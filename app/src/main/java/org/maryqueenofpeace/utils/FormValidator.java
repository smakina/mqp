package org.maryqueenofpeace.utils;

import android.app.Activity;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import org.maryqueenofpeace.R;

import java.util.ArrayList;
import java.util.List;


public class FormValidator {
    private Activity activity;

    public FormValidator(Activity activity) {
        this.activity = activity;
    }

    public boolean validateInput(EditText... editTexts) {
        boolean isValid = true;

        for (EditText editText : editTexts) {
            String v = editText.getText().toString();

            if(!((View) editText.getParent()).isShown()){
                continue;
            }

            if (v.equals("") || v.isEmpty()) {
                editText.setError(activity.getString(R.string.required));
                isValid = false;
            }
        }
        return isValid;
    }

    public static boolean inputsEqual(EditText et1, EditText et2 ){
        boolean isEqual = et1.getText().toString().equals(et2.getText().toString());
        if (!isEqual) {
            et1.setError(et1.getContext().getString(R.string.miss_match));
            et2.setError(et1.getContext().getString(R.string.miss_match));
        }
        return isEqual;
    }


    public static boolean validateInputs(EditText... editTexts) {
        boolean isValid = true;

        for (EditText editText : editTexts) {
            String v = editText.getText().toString();

            if(!((View) editText.getParent()).isShown()){
                continue;
            }

            if (v.equals("") || v.isEmpty()) {
                editText.setError(editText.getContext().getString(R.string.required));
                isValid = false;
            }
        }
        return isValid;
    }
    public static boolean validateInputs(TextInputLayout... textInputLayouts) {
        boolean isValid = false;

        for (TextInputLayout textInputLayout : textInputLayouts) {
            EditText editText = textInputLayout.getEditText();
            assert editText != null;
            String v = editText.getText().toString();
            if(!((View) editText.getParent()).isShown()){
                continue;
            }

            if (v.equals("") || v.isEmpty()) {
                textInputLayout.setError(editText.getContext().getString(R.string.required));
                isValid = true;
            }
        }
        return isValid;
    }

//    @SafeVarargs
//    public static boolean validateSelect( SmartMaterialSpinner<String>... smartMaterialSpinners) {
//        boolean isValid = true;
//
//        for (SmartMaterialSpinner<String> smartMaterialSpinner : smartMaterialSpinners) {
//            if (smartMaterialSpinner.getSelectedItem() == null) {
//                smartMaterialSpinner.setErrorText(smartMaterialSpinner.getContext().getString(R.string.you_must_select_an_item));
//                isValid = false;
//            }
//        }
//        return isValid;
//    }

    public boolean unique(String tableName, EditText... editTexts) {
        boolean isValid = true;

        for (EditText editText : editTexts) {
            String v = editText.getText().toString();

            if (v.equals("") || v.isEmpty()) {
                editText.setError(activity.getString(R.string.required));
                isValid = false;
            }
        }
        return isValid;
    }

    public List<String> validateSpinner(Spinner... spinners) {
        List<String> error = new ArrayList<>();

        for (Spinner spinner : spinners) {
//            log(spinner.isSelected() + "");
            if (!spinner.isSelected()) {
                Object t = spinner.getTag();
                if (t != null) {
                    error.add(t.toString());
                }
            }
        }
        return error;
    }

    public String getInputData(TextInputEditText editText) {
        Object et = editText.getText();
        if (et == null) return "";
        else return et.toString();
    }

    public String getInputData(EditText editText) {
        Object et = editText.getText();
        if (et == null) return "";
        else return et.toString();
    }

    public String getSelectedData(Spinner spinner) {
        Object it = spinner.getSelectedItem();
        if (it == null) return "";
        else return spinner.getSelectedItem().toString();
    }

    public String getCheckedData(RadioGroup radioGroup){
        int checkedId = radioGroup.getCheckedRadioButtonId();
        RadioButton radioButton = radioGroup.findViewById(checkedId);
        if (radioButton == null) return "";
        else return radioButton.getText().toString();
    }

    public boolean confirmIfEqual(TextInputEditText...editTexts){
        for (TextInputEditText editText : editTexts){
            for (TextInputEditText et : editTexts){
                if (!et.getText().toString().equals(editText.getText().toString()))
                    return false;
            }
        }
        return true;
    }
}
