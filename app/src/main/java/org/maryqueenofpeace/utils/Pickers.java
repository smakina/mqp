package org.maryqueenofpeace.utils;


import androidx.core.util.Pair;
import androidx.fragment.app.FragmentManager;

import com.google.android.material.datepicker.MaterialDatePicker;

import org.maryqueenofpeace.DatePickerInterface;
import org.maryqueenofpeace.R;

public class Pickers {

    public static <T> void getDateRagePicker(FragmentManager fm, DatePickerInterface<Pair<Long, Long>> datePickerInterface) {
        MaterialDatePicker<Pair<Long, Long>> datePicker = MaterialDatePicker.Builder.dateRangePicker().setTheme(R.style.MaterialCalendarTheme).build();
        datePicker.addOnPositiveButtonClickListener(datePickerInterface::onDateSelected);
        datePicker.show(fm, null);
    }

}
