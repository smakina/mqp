package org.maryqueenofpeace;

import static org.maryqueenofpeace.HelperMethods.log;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.Manifest;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.WindowManager;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.DecelerateInterpolator;

import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;

import org.maryqueenofpeace.databinding.ActivitySplashBinding;

import java.util.List;

public class SplashActivity extends AppCompatActivity {


    private boolean creating = true;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_splash);

        Dexter.withContext(this).withPermissions(
                Manifest.permission.READ_CALL_LOG,
                Manifest.permission.WRITE_CONTACTS,
                Manifest.permission.READ_CONTACTS,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.READ_EXTERNAL_STORAGE
        )
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport multiplePermissionsReport) {
                        loadContact();
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> list, PermissionToken permissionToken) {
                        finish();
                    }
                }).check();

    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    private ActivitySplashBinding binding;


    private void loadContact() {
        App app = (App) getApplicationContext();
        binding.setIsLoading(true);

        app.loadContacts(new RepoInterface<String>() {
            @Override
            public void onSuccess(String s) {
                runOnUiThread(() -> {
                    binding.setIsLoading(false);
                    startActivity(new Intent(SplashActivity.this, MainActivity.class));
                    finish();
                });
            }
        });
    }
}