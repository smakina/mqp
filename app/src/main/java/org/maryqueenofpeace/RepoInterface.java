package org.maryqueenofpeace;


import android.content.Context;

public interface RepoInterface<T> {
    default void onStart(){};
    default void onSuccess(T t){};
    default void onSuccess(){};
    default void onComplete(){};
    default void onError(String message) {}
    default void onError(Context context,  String message) {
//        dialogError(context, "ERROR", message, null);
    }

}
