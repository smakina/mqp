package org.maryqueenofpeace.viewmodel;

import static org.maryqueenofpeace.HelperMethods.log;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import org.maryqueenofpeace.RepoInterface;
import org.maryqueenofpeace.model.ContactsModal;
import org.maryqueenofpeace.repo.ContactsRepo;

import java.io.IOException;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ContactsViewModel extends AndroidViewModel {
    private ContactsRepo contactsRepo;

    public ContactsViewModel(@NonNull Application application) {
        super(application);
        contactsRepo = new ContactsRepo(application);
    }

    public void insert(ContactsModal...contactsModals) {
        contactsRepo.insert(contactsModals);
    }

    public LiveData<List<ContactsModal>> getContacts() {
        return contactsRepo.getContacts();
    }

    public void saveData(String school_id, List<ContactsModal> contacts, RepoInterface<String> repoInterface) {
        contactsRepo.saveContacts(school_id, contacts).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        log(response.body().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    repoInterface.onSuccess("Success");
                } else {
                    try {
                        log(response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
                repoInterface.onError(t.getMessage());
            }
        });
    }

    public void loadApiContacts( RepoInterface<String> repoInterface) {
        contactsRepo.getApiContacts().enqueue(new Callback<List<ContactsModal>>() {
            @Override
            public void onResponse(Call<List<ContactsModal>> call, Response<List<ContactsModal>> response) {
                if (response.isSuccessful() && response.body() != null) {
                    ContactsModal[] contactsModals = response.body().toArray(new ContactsModal[0]);
                    log("Total contacts > "+contactsModals.length);
                    contactsRepo.insert(contactsModals);
                    repoInterface.onSuccess("Success");
                } else {
                    repoInterface.onError("Unknown error. please contact the developer: 0881960016");
                }
            }

            @Override
            public void onFailure(Call<List<ContactsModal>> call, Throwable t) {
                repoInterface.onError(t.getMessage());
            }
        });
    }
}
