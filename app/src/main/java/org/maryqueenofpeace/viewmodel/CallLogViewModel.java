package org.maryqueenofpeace.viewmodel;

import static org.maryqueenofpeace.HelperMethods.getContactName;
import static org.maryqueenofpeace.HelperMethods.log;

import android.app.Activity;
import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;

import org.maryqueenofpeace.ContactsHelper;
import org.maryqueenofpeace.HelperMethods;
import org.maryqueenofpeace.MainActivity;
import org.maryqueenofpeace.db.LocalDataBase;
import org.maryqueenofpeace.model.ContactsModal;
import org.maryqueenofpeace.model.MCallLog;
import org.maryqueenofpeace.repo.CallLogRepo;
import org.maryqueenofpeace.utils.LiveDataUtils;

import java.util.List;


public class CallLogViewModel extends AndroidViewModel {
    private List<ContactsModal> contactsModalList;
    private CallLogRepo callLogRepo;

    public CallLogViewModel(@NonNull Application application) {
        super(application);
        callLogRepo = new CallLogRepo(application);
    }

    public void insert(MCallLog... MCallLogs) {
        callLogRepo.insert(MCallLogs);
    }

    public LiveData<List<MCallLog>> getCallLogList() {
        return callLogRepo.getCallLog();
    }

    public LiveData<Integer> getCount(String filter) {
        return callLogRepo.getCount(filter);
    }

    public LiveData<Integer> getCountTotal() {
        return callLogRepo.getCountTotal();
    }

    public void saveContacts(Activity activity, List<ContactsModal> contactsModalList) {
        this.contactsModalList = contactsModalList;
        LiveDataUtils.observeOnce(getCallLogList(), mCallLogs -> {
            LocalDataBase.databaseWriterService.execute(() -> {
                log(" TEST Calls > "+mCallLogs.size());
                int contact = 0;

                for (MCallLog mCallLog : mCallLogs) {

                    log(getContactName()+" "+mCallLog.phone);

                    if (!ifExists(mCallLog.phone)) {
                        ContactsHelper.saveContact(activity, getContactName()+contact, mCallLog.phone);
                        contact+=1;
                    }

                }
            });
        });
    }

    private boolean ifExists(String phone) {
        for (ContactsModal cm : contactsModalList) {
            if (phone.equals(cm.contactNumber)) {
                return true;
            }
        }
        return false;
    }

}
