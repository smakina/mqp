package org.maryqueenofpeace;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import android.widget.LinearLayout;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import org.maryqueenofpeace.utils.LineItemDecoration;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class HelperMethods {

    public static File getFile(Context context) {

        File imagePath = new File(context.getFilesDir(), "my_images");
        File newFile = new File(imagePath, "default_image.jpg");
//        Uri contentUri = getUriForFile(context, "com.mydomain.fileprovider", newFile);

        log(imagePath.getPath());
        imagePath.mkdir();

//        File base_path = new File(context.getExternalFilesDir(null), "TESTING");
//       Uri uri = FileProvider.getUriForFile(context, "com.example.calllogger.fileprovider", base_path);
//
//        base_path.mkdir();
//
//        File newFile = new File(base_path, "contacts.txt");
//        try {
////            log("Creating file");
//            newFile.createNewFile();
//            log("File created");
//        } catch (IOException e) {
//            log(e.getMessage());
//            e.printStackTrace();
//        }

//        return getUriForFile(context, "com.example.login2", newFile);
        return newFile;
    }

    public static void writeToFile(String data,Context context) {
        File file = getFile(context);
        saveContents(data, file);
    }

    public static void saveContents(String contents, File file) {
        try {
            Log.d("EK", "saveContents: "+file.getPath());
            BufferedWriter bw = new BufferedWriter(new FileWriter(file));
            bw.write(contents);
            bw.close();
        } catch (IOException e) {
            Log.d("EK", "saveContents: --- "+e.getMessage());
            e.printStackTrace();
        }
    }

    public static void log(Object object){
        Log.d("EK", ""+object);
    }

    public static void configRecy(RecyclerView recyclerView, boolean underline){
        recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext()));
        if (underline)
            recyclerView.addItemDecoration(new LineItemDecoration(recyclerView.getContext(), LinearLayout.VERTICAL));
        recyclerView.setHasFixedSize(true);
    }


    /**
     * @return yyyy-MM-dd HH:mm:ss formate date as string
     */
    public static String formatDate(String string) {
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss", Locale.getDefault());
            SimpleDateFormat newFormat = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault());
            Date date = dateFormat.parse(string);
            return newFormat.format(date);
        } catch (Exception e) {
            log(string);
            log(e.getMessage());
            e.printStackTrace();
            return "";
        }
    }

    /**
     * @return yyyy-MM-dd HH:mm:ss formate date as string
     */
    public static String formatDate(long dt) {
        try {
//            SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss", Locale.getDefault());
            SimpleDateFormat newFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm", Locale.getDefault());
            Date date = new Date(dt);
            return newFormat.format(date);
        } catch (Exception e) {
            log(e.getMessage());
            e.printStackTrace();
            return "";
        }
    }

    public static String getContactName() {
        try {
//            SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss", Locale.getDefault());
            SimpleDateFormat newFormat = new SimpleDateFormat("ddMMyyyy_HHmmss", Locale.getDefault());
            Date date = new Date();
            return "Contact_"+newFormat.format(date);
        } catch (Exception e) {
            log(e.getMessage());
            e.printStackTrace();
            return "";
        }
    }

    public static int IMAGE_SAVE_INTENT_CODE = 1000;

    public static void saveFileDialog(Activity activity){
        Intent intent = new Intent(Intent.ACTION_CREATE_DOCUMENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        intent.setType("text/csv"); //not needed, but maybe usefull
//        intent.setType("application/vnd.ms-excel"); //not needed, but maybe usefull
        intent.putExtra(Intent.EXTRA_TITLE, "contacts.csv"); //not needed, but maybe usefull
        activity.startActivityForResult(intent, IMAGE_SAVE_INTENT_CODE);
    }

    public static String formatPhoneNumber(String number){
        if (number.length() == 13) {
            String code = number.substring(0, 4);
            String chank1 = number.substring(4, 7);
            String chank2 = number.substring(7, 10);
            String chank3 = number.substring(10, 13);
            return code+" "+chank1+" "+chank2+" "+chank3;
            // +265 881 960 016
        } else if (number.length() == 10){
            String code = "+265";
            String chank1 = number.substring(1, 4);
            String chank2 = number.substring(4, 7);
            String chank3 = number.substring(7, 10);
            return code+" "+chank1+" "+chank2+" "+chank3;
        }
        return number;
    }

}
