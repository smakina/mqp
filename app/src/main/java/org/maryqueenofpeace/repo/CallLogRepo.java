package org.maryqueenofpeace.repo;

import android.app.Application;

import androidx.lifecycle.LiveData;

import org.maryqueenofpeace.db.LocalDataBase;
import org.maryqueenofpeace.db.dao.CallLogDao;
import org.maryqueenofpeace.db.retrofit.ApiInterface;
import org.maryqueenofpeace.db.retrofit.RetrofitUtility;
import org.maryqueenofpeace.model.MCallLog;

import java.util.List;


public class CallLogRepo {
    private CallLogDao callLogDao;
    private ApiInterface apiInterface;

    public CallLogRepo(Application application) {
        callLogDao = LocalDataBase.getRoomDatabase(application).userDao();
        apiInterface = RetrofitUtility.getRetrofitClient(application).create(ApiInterface.class);
    }

    public LiveData<List<MCallLog>> getCallLog() {
        return callLogDao.getCallLog();
    }

    public void insert(MCallLog... MCallLogs) {
        LocalDataBase.databaseWriterService.execute(()->{
            callLogDao.insert(MCallLogs);
        });
    }

    public LiveData<Integer> getCount(String filter) {
        return callLogDao.getCount(filter);
    }

    public LiveData<Integer> getCountTotal() {
        return callLogDao.getCountTotal();
    }
}
