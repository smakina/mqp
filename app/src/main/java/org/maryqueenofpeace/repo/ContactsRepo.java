package org.maryqueenofpeace.repo;

import static org.maryqueenofpeace.HelperMethods.log;

import android.app.Application;

import androidx.lifecycle.LiveData;

import com.google.gson.Gson;

import org.maryqueenofpeace.db.LocalDataBase;
import org.maryqueenofpeace.db.dao.ContactsDao;
import org.maryqueenofpeace.db.retrofit.ApiInterface;
import org.maryqueenofpeace.db.retrofit.RetrofitUtility;
import org.maryqueenofpeace.model.ContactsModal;

import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;

public class ContactsRepo {
    private ApiInterface apiInterface;
    private ContactsDao contactsDao;

    public ContactsRepo(Application application) {
        contactsDao = LocalDataBase.getRoomDatabase(application).contactsDao();
        apiInterface = RetrofitUtility.getRetrofitClient(application).create(ApiInterface.class);
    }

    public void insert(ContactsModal...contactsModals){
        LocalDataBase.databaseWriterService.execute(() -> contactsDao.insert(contactsModals));
    }

    public LiveData<List<ContactsModal>> getContacts() {
        return contactsDao.getContacts();
    }

    public Call<List<ContactsModal>> getApiContacts() {
        return apiInterface.getContacts();
    }

    public Call<ResponseBody> saveContacts(String school_id, List<ContactsModal> contacts) {
        List<String> contactList = new ArrayList<>();
        Gson g = new Gson();
        for (ContactsModal contactsModal : contacts) {
            contactList.add(g.toJson(contactsModal));
        }
        return apiInterface.saveData(school_id, contactList);
    }
}