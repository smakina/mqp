package org.maryqueenofpeace.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.ListAdapter;
import androidx.recyclerview.widget.RecyclerView;

import org.maryqueenofpeace.R;
import org.maryqueenofpeace.databinding.ItemCallBinding;
import org.maryqueenofpeace.model.MCallLog;

import java.util.ArrayList;
import java.util.List;

public class CallHistoryAdapter extends ListAdapter<MCallLog, CallHistoryAdapter.CallHistoryViewHolder> {
    //  implements Filterable
    public OnItemClickListener mOnItemClickListener;
    public Context ctx;
    public boolean maltSelect = false;

    public interface OnItemClickListener {
        void onItemClick(View view, MCallLog obj);
    }

    public void setOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.mOnItemClickListener = mItemClickListener;
    }

    public CallHistoryAdapter(@NonNull DiffUtil.ItemCallback<MCallLog> diffCallback) {
        super(diffCallback);
    }

    @Override
    public CallHistoryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ItemCallBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_call, parent, false);
        return new CallHistoryViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(CallHistoryViewHolder holder, int position) {
        MCallLog current = getItem(position);
        holder.bind(current);
    }

    public static class callHistoryDiff extends DiffUtil.ItemCallback<MCallLog> {
        @Override
        public boolean areItemsTheSame(@NonNull MCallLog oldItem, @NonNull MCallLog newItem) {
            return oldItem == newItem;
        }

        @Override
        public boolean areContentsTheSame(@NonNull MCallLog oldItem, @NonNull MCallLog newItem) {
            return oldItem.id == newItem.id;
        }
    }

    class CallHistoryViewHolder extends RecyclerView.ViewHolder {
        private ItemCallBinding binding;

        private CallHistoryViewHolder(ItemCallBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        public void bind(MCallLog mCallLog) {
            binding.setLog(mCallLog);
            int res = mCallLog.type.equals("INCOMING") ? R.drawable.ic_baseline_check_circle_24 : R.drawable.ic_missed_call;
            binding.stateIcon.setImageResource(res);
        }
    }

}
