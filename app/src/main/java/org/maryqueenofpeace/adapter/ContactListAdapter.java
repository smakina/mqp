package org.maryqueenofpeace.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.ListAdapter;
import androidx.recyclerview.widget.RecyclerView;

import org.maryqueenofpeace.R;
import org.maryqueenofpeace.databinding.ItemCallBinding;
import org.maryqueenofpeace.databinding.ItemContactBinding;
import org.maryqueenofpeace.model.ContactsModal;
import org.maryqueenofpeace.model.MCallLog;

public class ContactListAdapter  extends ListAdapter<ContactsModal, ContactListAdapter.contactViewHolder> {
    //  implements Filterable
    public OnItemClickListener mOnItemClickListener;
    public Context ctx;
    public boolean maltSelect = false;

    public interface OnItemClickListener {
        void onItemClick(View view, MCallLog obj);
    }

    public void setOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.mOnItemClickListener = mItemClickListener;
    }

    public ContactListAdapter(@NonNull DiffUtil.ItemCallback<ContactsModal> diffCallback) {
        super(diffCallback);
    }

    @Override
    public contactViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ItemContactBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_contact, parent, false);
        return new contactViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(contactViewHolder holder, int position) {
        ContactsModal current = getItem(position);
        holder.bind(current);
    }

    public static class contactListDiff extends DiffUtil.ItemCallback<ContactsModal> {
        @Override
        public boolean areItemsTheSame(@NonNull ContactsModal oldItem, @NonNull ContactsModal newItem) {
            return oldItem == newItem;
        }

        @Override
        public boolean areContentsTheSame(@NonNull ContactsModal oldItem, @NonNull ContactsModal newItem) {
            return oldItem.contactNumber.equals(newItem.contactNumber);
        }
    }

    class contactViewHolder extends RecyclerView.ViewHolder {
        private ItemContactBinding binding;

        private contactViewHolder(ItemContactBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        public void bind(ContactsModal mCallLog) {
            binding.setLog(mCallLog);
//            int res = mCallLog.type.equals("INCOMING") ? R.drawable.ic_baseline_check_circle_24 : R.drawable.ic_missed_call;
//            binding.stateIcon.setImageResource(res);
        }
    }

}
