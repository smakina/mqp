package org.maryqueenofpeace;

import static org.maryqueenofpeace.utils.FilePath.getPath;

import android.content.Context;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;

import com.opencsv.CSVWriter;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

public class CSVHelper {

    //    public static void saveToCSV(Context context) throws IOException {
////        File file = new File(csv, "test.csv");
////        File csv = new File(Environment.getExternalStorageDirectory(), "test.csv");
////        Uri uri = FileProvider.getUriForFile(context, context.getPackageName()+".fileprovider", csv);
//
//        File newFile = new File(commonDocumentDirPath("Test"), "contacts.csv");
//        Uri contentUri = FileProvider.getUriForFile(context, context.getPackageName()+".fileprovider", newFile);
//
//        // create the parent files if necessary
//
////        File file = new File(contentUri.getPath());
//        if (!newFile.exists())
//            newFile.createNewFile();
//
//        CSVWriter writer = new CSVWriter(new FileWriter(newFile));
//
//
//        List<String[]> data = new ArrayList<String[]>();
//        data.add(new String[] {"India", "New Delhi"});
//        data.add(new String[] {"United States", "Washington D.C"});
//        data.add(new String[] {"Germany", "Berlin"});
//
//        writer.writeAll(data);
//
//        writer.close();
//    }
    public static void saveToCSV(Context context, List<String[]> data, Uri uri) throws IOException {
        String path = getPath(context, uri);
        if (path == null) return;
//
        File newFile = new File(path);
//        File newFile = new File(commonDocumentDirPath("Test"), "contacts.csv");
//        Uri contentUri = FileProvider.getUriForFile(context, context.getPackageName()+".fileprovider", newFile);

        CSVWriter writer = new CSVWriter(new FileWriter(newFile));

//        List<String[]> data = new ArrayList<String[]>();
//        data.add(new String[] {"India", "New Delhi"});
//        data.add(new String[] {"United States", "Washington D.C"});
//        data.add(new String[] {"Germany", "Berlin"});

        writer.writeAll(data);

        writer.close();
    }

    public static File commonDocumentDirPath(String FolderName) {
        File dir = null;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            dir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS) + "/" + FolderName);
        } else {
            dir = new File(Environment.getExternalStorageDirectory() + "/" + FolderName);
        }

        // Make sure the path directory exists.
        if (!dir.exists()) {
            // Make it, if it doesn't exit
            boolean success = dir.mkdirs();
            if (!success) {
                dir = null;
            }
        }
        return dir;
    }


//    private void getContacts() {
//        // this method is use to read contact from users device.
//        // on below line we are creating a string variables for
//        // our contact id and display name.
//        String contactId = "";
//        String displayName = "";
//        // on below line we are calling our content resolver for getting contacts
//        Cursor cursor = getContentResolver().query(ContactsContract.Contacts.CONTENT_URI, null, null, null, ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME + " ASC");
//        // on blow line we are checking the count for our cursor.
//        if (cursor.getCount() > 0) {
//            // if the count is greater than 0 then we are running a loop to move our cursor to next.
//            while (cursor.moveToNext()) {
//                // on below line we are getting the phone number.
//                int hasPhoneNumber = Integer.parseInt(cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER)));
//                if (hasPhoneNumber > 0) {
//                    // we are checking if the has phone number is > 0
//                    // on below line we are getting our contact id and user name for that contact
//                    contactId = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts._ID));
//                    displayName = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
//                    // on below line we are calling a content resolver and making a query
//                    Cursor phoneCursor = getContentResolver().query(
//                            ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
//                            null,
//                            ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?",
//                            new String[]{contactId},
//                            null);
//                    // on below line we are moving our cursor to next position.
//                    if (phoneCursor.moveToNext()) {
//                        // on below line we are getting the phone number for our users and then adding the name along with phone number in array list.
//                        String phoneNumber = phoneCursor.getString(phoneCursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
//                        contactsModalArrayList.add(new ContactsModal(displayName, phoneNumber));
//                    }
//                    // on below line we are closing our phone cursor.
//                    phoneCursor.close();
//                }
//            }
//        }
//        // on below line we are closing our cursor.
//        cursor.close();
//        // on below line we are hiding our progress bar and notifying our adapter class.
//        loadingPB.setVisibility(View.GONE);
//        contactRVAdapter.notifyDataSetChanged();
//    }
//}
}
