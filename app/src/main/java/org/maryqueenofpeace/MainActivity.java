package org.maryqueenofpeace;

import static org.maryqueenofpeace.CSVHelper.commonDocumentDirPath;
import static org.maryqueenofpeace.CSVHelper.saveToCSV;
import static org.maryqueenofpeace.ContactsHelper.getData1;
import static org.maryqueenofpeace.ContactsHelper.saveContact;
import static org.maryqueenofpeace.HelperMethods.IMAGE_SAVE_INTENT_CODE;
import static org.maryqueenofpeace.HelperMethods.configRecy;
import static org.maryqueenofpeace.HelperMethods.formatPhoneNumber;
import static org.maryqueenofpeace.HelperMethods.log;

import android.Manifest;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.os.Handler;
import android.provider.CallLog;
import android.telephony.PhoneNumberUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.core.util.Pair;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.google.android.material.datepicker.MaterialDatePicker;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;

import org.maryqueenofpeace.adapter.CallHistoryAdapter;
import org.maryqueenofpeace.databinding.ActivityMainBinding;
import org.maryqueenofpeace.db.LocalDataBase;
import org.maryqueenofpeace.model.ContactsModal;
import org.maryqueenofpeace.model.MCallLog;
import org.maryqueenofpeace.utils.Dialog;
import org.maryqueenofpeace.utils.Pickers;
import org.maryqueenofpeace.utils.Tools;
import org.maryqueenofpeace.utils.ViewAnimation;
import org.maryqueenofpeace.viewmodel.CallLogViewModel;
import org.maryqueenofpeace.viewmodel.ContactsViewModel;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


public class MainActivity extends AppCompatActivity {

    private List<ContactsModal> contactsModalList;

    private boolean rotate = false;
    private CallLogViewModel callLogViewModel;
    private ActivityMainBinding binding;
    private CallHistoryAdapter callHistoryAdapter;
    private ContactsViewModel contactsViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        callLogViewModel = new ViewModelProvider(this).get(CallLogViewModel.class);
        contactsViewModel = new ViewModelProvider(this).get(ContactsViewModel.class);

        loadAllContacts();

        initToolbar();
        initComponents();
        loadData();
        loadContacts();
        loadApiContacts();
    }

    private void loadAllContacts() {
        binding.loader.setVisibility(View.VISIBLE);
        LocalDataBase.databaseWriterService.execute(() -> {
            contactsModalList = ContactsHelper.getContacts(MainActivity.this);
            runOnUiThread(() -> {
                binding.loader.setVisibility(View.GONE);
            });
        });
    }

//    private void setButton() {
//        ViewAnimation.showOut(binding.fabMic);
//        ViewAnimation.showOut(binding.fabCall);
//
//        binding.fabAdd.setOnClickListener(v -> {
//           open();
//        });
//    }

//    private void open() {
//        rotate = ViewAnimation.rotateFab(binding.fabAdd, !rotate);
//
//        if (rotate) {
//            ViewAnimation.showIn(binding.fabMic);
//            ViewAnimation.showIn(binding.fabCall);
//        } else {
//            ViewAnimation.showOut(binding.fabMic);
//            ViewAnimation.showOut(binding.fabCall);
//        }
//
//    }

    private void initComponents() {

        configRecy(binding.rcHistory, false);
        callHistoryAdapter = new CallHistoryAdapter(new CallHistoryAdapter.callHistoryDiff());
        binding.rcHistory.setAdapter(callHistoryAdapter);

        binding.ltySaveContacts.setOnClickListener(view -> {
            binding.setIsLoading(true);
            callLogViewModel.saveContacts(MainActivity.this, contactsModalList);
            new Handler().postDelayed(() -> binding.setIsLoading(false), 2500);
        });

        binding.ltyExportContacts.setOnClickListener(view -> {
            HelperMethods.saveFileDialog(MainActivity.this);
        });

        binding.ltySyncContacts.setOnClickListener(view -> {
            binding.setIsLoading(true);
            contactsViewModel.saveData("1", App.contactsModalList, new RepoInterface<String>() {
                @Override
                public void onSuccess(String s) {
                    binding.setIsLoading(false);
                    Dialog.dialogInfo(MainActivity.this, "Success", "All contacts have been successfully uploaded", null);
                }

                @Override
                public void onError(String message) {
                    binding.setIsLoading(false);
                    Dialog.dialogError(MainActivity.this, "Connection Error", message, null);
                }
            });
        });

        binding.ltyViewContacts.setOnClickListener(view -> {
            startActivity(new Intent(MainActivity.this, ContactListActivity.class));
        });

        binding.fabAdd.setOnClickListener(view -> {
            loadApiContacts();
        });
    }

    private void loadApiContacts() {
        binding.setIsLoading(true);
        contactsViewModel.loadApiContacts(new RepoInterface<String>() {
            @Override
            public void onSuccess(String s) {
                binding.setIsLoading(false);
            }

            @Override
            public void onError(String message) {
                binding.setIsLoading(false);
                Dialog.dialogError(MainActivity.this, "Connection Error", message, null);
            }
        });
    }


    private void loadData() {
//        callLogViewModel.getCallLogList().observe(this, mCallLogs -> {
//            callHistoryAdapter.submitList(mCallLogs);
//        });

//        callLogViewModel.getCount("INCOMING").observe(this, integer -> {
//            binding.tvAnswered.setText(String.valueOf(integer));
//        });
//
//        callLogViewModel.getCount("MISSED").observe(this, integer -> {
//            binding.tvMissed.setText(String.valueOf(integer));
//        });

        callLogViewModel.getCountTotal().observe(this, integer -> {
            binding.tvTotal.setText(String.valueOf(integer));
        });
    }

    private void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
//        toolbar.getNavigationIcon().setColorFilter(getResources().getColor(R.color.deep_purple_500), PorterDuff.Mode.SRC_ATOP);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(null);
//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        Tools.setSystemBarColor(this);
//        Tools.set(this);
//        Tools.setSystemBarLight(this);

    }

    private final DatePickerInterface<Pair<Long, Long>> datePickerInterface = pair -> {
        Long startDate = pair.first;
        Long endDate = pair.second;

        // Load data from database
    };

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_search_setting, menu);
        Tools.changeMenuIconColor(menu, getResources().getColor(R.color.colorPrimary));
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        } else if (item.getItemId() == R.id.action_contact_list){
            startActivity(new Intent(MainActivity.this, ContactListActivity.class));
        }
        return super.onOptionsItemSelected(item);
    }

    private void loadContacts() {
//        Dexter.withContext(this).withPermissions(
//                Manifest.permission.READ_CALL_LOG,
//                Manifest.permission.WRITE_CONTACTS,
//                Manifest.permission.READ_CONTACTS,
//                Manifest.permission.WRITE_EXTERNAL_STORAGE ,
//                Manifest.permission.READ_EXTERNAL_STORAGE).withListener(new MultiplePermissionsListener() {
//            @Override
//            public void onPermissionsChecked(MultiplePermissionsReport multiplePermissionsReport) {
//                if (multiplePermissionsReport.areAllPermissionsGranted()) {
//                    getCallDetails();
//                }
//            }
//
//            @Override
//            public void onPermissionRationaleShouldBeShown(List<PermissionRequest> list, PermissionToken permissionToken) {
//
//            }
//        }).check();

    }

    private List<MCallLog> getCallDetails() {
        List<MCallLog> mCallLogList = new ArrayList<>();

        Cursor managedCursor = getContentResolver().query(CallLog.Calls.CONTENT_URI, null,
                null, null, null);
        int number = managedCursor.getColumnIndex(CallLog.Calls.NUMBER);
        int type = managedCursor.getColumnIndex(CallLog.Calls.TYPE);
        int date = managedCursor.getColumnIndex(CallLog.Calls.DATE);
        int duration = managedCursor.getColumnIndex(CallLog.Calls.DURATION);

        StringBuilder sb = new StringBuilder();

        while (managedCursor.moveToNext()) {
            String phNumber = managedCursor.getString(number);
            String callType = managedCursor.getString(type);
            long callDate = managedCursor.getLong(date);
            String callDuration = managedCursor.getString(duration);
            String dir = null;

            int dircode = Integer.parseInt(callType);

            switch (dircode) {
                case CallLog.Calls.OUTGOING_TYPE:
                    dir = "OUTGOING";
                    break;

                case CallLog.Calls.INCOMING_TYPE:
                    dir = "INCOMING";
                    break;

                case CallLog.Calls.MISSED_TYPE:
                    dir = "MISSED";
                    break;
            }

            sb.append(phNumber).append("\n");
            mCallLogList.add(new MCallLog(formatPhoneNumber(phNumber), dir, callDate, ""+callDuration ));
        }

//        HelperMethods.writeToFile(sb.toString(), this);
        managedCursor.close();

        MCallLog[] mCallLogs = mCallLogList.toArray(new MCallLog[0]);
        callLogViewModel.insert(mCallLogs);

        return mCallLogList;
    }

    private void toastIconInfo() {
        Toast toast = new Toast(getApplicationContext());
        toast.setDuration(Toast.LENGTH_LONG);

        //inflate view
        View custom_view = getLayoutInflater().inflate(R.layout.toast_icon_text, null);
        ((TextView) custom_view.findViewById(R.id.message)).setText("CSV file is exported successfully!");
        ((ImageView) custom_view.findViewById(R.id.icon)).setImageResource(R.drawable.ic_error_outline);
        ((CardView) custom_view.findViewById(R.id.parent_view)).setCardBackgroundColor(getResources().getColor(R.color.blue_500));

        toast.setView(custom_view);
        toast.show();
    }

//    saveContact(this, "Test Contact", "0928827736");


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == IMAGE_SAVE_INTENT_CODE) {
                try {
                    CSVHelper.saveToCSV(MainActivity.this, getData1(App.contactsModalList), data.getData());
                    toastIconInfo();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}