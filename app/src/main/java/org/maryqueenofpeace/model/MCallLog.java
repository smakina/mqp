package org.maryqueenofpeace.model;


import androidx.room.Entity;
import androidx.room.Index;
import androidx.room.PrimaryKey;

@Entity(tableName = "tbl_cal_log", indices = {@Index(value = {"phone"}, unique = true)})
public class MCallLog {
    @PrimaryKey(autoGenerate = true)
    public int id;
    public String phone;
    public String type;
    public long date;
    public String duration;

    public MCallLog(String phone, String type, long date, String duration) {
        this.phone = phone;
        this.type = type;
        this.date = date;
        this.duration = duration;
    }
}
