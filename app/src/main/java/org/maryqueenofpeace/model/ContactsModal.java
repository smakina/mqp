package org.maryqueenofpeace.model;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

@Entity(tableName = "tbl_contacts")
public class ContactsModal implements Serializable {
    @NonNull
    @PrimaryKey
    @SerializedName("phone")
    public String contactNumber;
    @SerializedName("name")
    public String userName;
    public boolean syncState;

    // constructor
    public ContactsModal(String userName, @NonNull String contactNumber) {
        this.userName = userName;
        this.contactNumber = contactNumber;
    }


}
