package org.maryqueenofpeace;

public interface DatePickerInterface<T> {
    void onDateSelected(T t);
}
