package org.maryqueenofpeace;

import static org.maryqueenofpeace.HelperMethods.configRecy;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.os.Bundle;
import android.view.View;

import org.maryqueenofpeace.adapter.CallHistoryAdapter;
import org.maryqueenofpeace.adapter.ContactListAdapter;
import org.maryqueenofpeace.databinding.ActivityContactListBinding;
import org.maryqueenofpeace.db.LocalDataBase;
import org.maryqueenofpeace.model.ContactsModal;

import java.util.List;

public class ContactListActivity extends AppCompatActivity {

    private ActivityContactListBinding binding;
    private ContactListAdapter contactListAdapter;
    private List<ContactsModal> contactsModalList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_contact_list);
        initComponents();
    }

    private void initComponents() {
        configRecy(binding.rcCallList, false);
        contactListAdapter = new ContactListAdapter(new ContactListAdapter.contactListDiff());
        binding.rcCallList.setAdapter(contactListAdapter);
        loadAllContacts();
    }

    private void loadAllContacts() {
//        binding.loader.setVisibility(View.VISIBLE);
        contactListAdapter.submitList(App.contactsModalList);
//        binding.loader.setVisibility(View.GONE);


    }

}